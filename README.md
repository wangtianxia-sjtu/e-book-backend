# e-book-backend

SE228 - Backend Part

## Usage

```bash
./mvnw spring-boot:run
```

# Week 6's plan

1. 针对之前编写的图书浏览等页面，编写后台Servlet，用户在前端页面点击“图书浏览”按钮后，Servlet会响应，并发送回详细的图书清单信息。

2. 图书清单信息应该与之前纯静态页面的版本保持一致，即原来在前端页面中固定的图书信息用Servlet回传。

3. 如果你还不知道如何在前端页面中显示Servlet回传的信息，那么你可以直接从Servlet回传包含图书信息的页面。即，首页有一个“浏览”按钮，点击后会跳转到Servlet回传的页面，显示图书信息。