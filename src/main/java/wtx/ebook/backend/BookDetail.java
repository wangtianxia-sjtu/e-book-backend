package wtx.ebook.backend;

public class BookDetail {
    private String isbn;
    private String name;
    private double price;
    private String author;
    private int inventory;
    private String cover;
    public BookDetail(String isbn,String name,double price,String author,int inventory,String cover)
    {
        this.isbn = isbn;
        this.name = name;
        this.price = price;
        this.author = author;
        this.inventory = inventory;
        this.cover = cover;
    }

    public String getIsbn()
    {
        return isbn;
    }

    public String getName()
    {
        return name;
    }

    public double getPrice()
    {
        return price;
    }

    public String getAuthor()
    {
        return author;
    }

    public int getInventory()
    {
        return inventory;
    }

    public String getCover()
    {
        return cover;
    }
}