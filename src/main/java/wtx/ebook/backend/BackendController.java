package wtx.ebook.backend;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.*;
import java.io.*;

@RestController
@CrossOrigin
public class BackendController {
    @RequestMapping("/bookdetail")
    public ArrayList<BookDetail> getBookDetail(@RequestParam(value="key", defaultValue="excited!") String key)
    {
        ArrayList<BookDetail> result = new ArrayList<BookDetail>();
        result.add(new BookDetail("9787119113920","习近平谈治国理政第一卷（2018再版）",56.8,"习近平",666,"http://cpc.people.com.cn/NMediaFile/2018/1213/MAIN201812131647000425109205218.gif"));
        result.add(new BookDetail("9787119113920","习近平谈治国理政第一卷（2018再版）",56.8,"习近平",666,"http://cpc.people.com.cn/NMediaFile/2018/1213/MAIN201812131647000425109205218.gif"));
        result.add(new BookDetail("9787119113920","习近平谈治国理政第一卷（2018再版）",56.8,"习近平",666,"http://cpc.people.com.cn/NMediaFile/2018/1213/MAIN201812131647000425109205218.gif"));
        return result;
    }
}